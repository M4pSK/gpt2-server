var async = require('async');
var await = require('await');
const { JsonDB } = require('node-json-db');
const { Config } = require('node-json-db/dist/lib/JsonDBConfig')
const cron = require('node-cron');
const Binance = require('binance-api-node').default
require('dotenv').config();
const dbTradeStatus = require('./dataRun/dbTradeStatus.json');
const dbRun0 = require('./dataRun/dbRun0.json');
const fs = require('fs');
const Moment = require('moment');


function hedgerBNB() {
    //! “At minute 0, 5, 10, 30, 35, 40, 45, 50, and 55 on day-of-month 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 23, 24, 26, 28, and 30.”
    //* Explicitly written
    cron.schedule('1,3,5,7,9,11,13,15,17,19,30,31,33,35,37,39,41,43,45,47,49,50,51,53,55,57,59 * * * *', () => {
        // cron.schedule('0,5,10,30,35,40,45,50,55 * 2,4,6,8,10,12,14,16,18,20,22,23,24,26,28,30 * *', () => {
        //? Authenticate Binance API
        const client = Binance()
        // Authenticated client, can make signed calls
        const client2 = Binance({
        apiKey: process.env.BNB_API_KEY,
        apiSecret: process.env.BNB_API_SECRET,
            // getTime: xxx // time generator function, optional, defaults to () => Date.now()
        })
        // client.time().then(time => console.log(time))

        //? Get Time
        //* read from dbTradeStatus.json
        async function getTimeHedger() {
            console.log('-------Hedger-------')
            //? Timestamp
            let timeStamp = Moment().format('lll');
            let unixStamp = Moment().unix();
            console.log('Time Stamp: ' + timeStamp)
            // console.log('Unix Stamp: ' + unixStamp)
        }

        //? Get TradeStatus + Price @ Trade
        let tradeStatusTrade = "";
        let hedgerStatus = "";
        async function getTradeStatusPrev() {
            fs.readFile('./dataRun/dbTradeStatus.json', function (err, data) {
                let json = JSON.parse(data)
                let json0 = JSON.parse(data)
                tradeStatusTrade = json.tradeStatus;
                hedgerStatus = json0.hedgerStatus;
            })
        }

        let tradePrice = "";
        //* Extract Price at previous trade
        async function getPriceTrade() {
            fs.readFile('./dataRun/dbTradeKeeper.json', function (err, data) {
                let json = JSON.parse(data)
                tradePrice = json.tradeKeeper;
            })
        }

        //? Get Previous Hedger Price
        //? Get Price Movement
        let priceMovePrevious = 0;
        let prevPrice = 0;
        async function getPriceMove() {
            fs.readFile('./dataRun/dbPriceMove.json', function (err, data) {
                let json = JSON.parse(data)
                let json0 = JSON.parse(data)
                priceMovePrevious = json.moveDiff;
                prevPrice = json0.hedgePrice
                console.log('Prev logged diff: ' + priceMovePrevious);
            })
        }

        let currentPrice = 0;
        async function getCurrentPrice() {
            let getPrice = await client.avgPrice({ symbol: 'BTCUSDT' });
            currentPrice = getPrice.price;
        }

        let priceMoveCurrent = 0;
        async function makePriceMove(pricePrev, PriceCurr) {
            let priceMoveCurrent = pricePrev - PriceCurr;
            console.log('Price Move Current: ' + priceMoveCurrent)
        }

        async function tradeStatus(tradeSts, prevTradeP, curTradeP) {
            async function tradeStatusReWrite(tReWrite, pReWrite, hedgerStsWrite, pMoveDiffWrite, pMoveCurrWirte) {
                let jsonTradeStatusReWrite = { "tradeStatus": tReWrite, "hedgerStatus": hedgerStsWrite }
                fs.writeFile("./dataRun/dbTradeStatus.json", JSON.stringify(jsonTradeStatusReWrite), (error) => {
                })

                let jsonPRiceReWrite = { "tradeKeeper": pReWrite }
                fs.writeFile("./dataRun/dbTradeKeeper.json", JSON.stringify(jsonPRiceReWrite), (error) => {
                })

                let jsonMoveDiffReWrite = { "moveDiff": pMoveDiffWrite, "hedgePrice": pReWrite }
                fs.writeFile("./dataRun/dbPriceMove.json", JSON.stringify(jsonMoveDiffReWrite), (error) => {
                })
            }

            async function hedgeMoveReWrite(pMoveDiff, pCurrentMove) {
                let jsonHedgerMoveReWrite = { "moveDiff": pMoveDiff, "hedgePrice": pCurrentMove }
                fs.writeFile("./dataRun/dbPriceMove.json", JSON.stringify(jsonHedgerMoveReWrite), (error) => {
                    console.log("Hedger Update");
                })
            }
            console.log('TradeStatus: ' + tradeStatusTrade)
            console.log('HedgerStatus: ' + hedgerStatus)
            console.log('Trade Price: ' + (Math.round(tradePrice * 100) / 100).toFixed(2))
            console.log('Hedger Price: ' + (Math.round(currentPrice * 100) / 100).toFixed(2))
            let pDiff = currentPrice - tradePrice

            //? IMPORTANT / CONFUSING: Takes the difference of the difference to de bounce API errors
            let priceMoveDiff = currentPrice - prevPrice

            console.log('Price Move: ' + (Math.round(priceMoveDiff * 100) / 100).toFixed(2))

            console.log('Price Diff: ' + (Math.round(pDiff * 100) / 100).toFixed(2))

            //? Catches price if it falls below a certain margin
            //? Regular Hourly Crypto Bot Runtime Strategy
            //? Check if Price Error
            if (currentPrice == 0) {
                console.log('Binance Price API Error: 0')
            }
            //? Debounces difference between price increase
            //* (Lower bounds deBounce)(Upper bounds deBounce)
            // else if ((priceMoveDiff > 33) || (priceMoveDiff < -33)) {
            //     console.log('API MISSFIRE 1 MIN VALUE OVER 40')
            // }
            //? Takes action only on hit
            else if (tradeStatusTrade == "0" && (pDiff < -40) && hedgerStatus == "open") {
                tradeStatusReWrite('1', currentPrice, 'hit', priceMoveDiff, currentPrice);
                const tradeBuy = require("./tradeBuy");
                tradeBuy(currentPrice);
                console.log('HIT BUY [Hold AT LIMIT]');
            }
            //! Adjustment of pCurrwrite to tradePrice
            else if (tradeStatusTrade == "0" && (pDiff > 15) && hedgerStatus == "open") {
                tradeStatusReWrite('0', currentPrice, 'miss', priceMoveDiff, tradePrice);
                // const tradeSell = require("./tradeSell")
                // tradeSell(currentPrice)
                console.log('MISS BUY [Hold AT LIMIT]');
            }
            else if (tradeStatusTrade == "1" && (pDiff > 40) && hedgerStatus == "open") {
                tradeStatusReWrite('0', currentPrice, 'hit', priceMoveDiff, currentPrice);
                const tradeSell = require("./tradeSell")
                tradeSell(currentPrice)
                console.log('HIT SELL [Hold AT LIMIT]');
            }
            //! Adjustment of pCurrwrite to tradePrice
            else if (tradeStatusTrade == "1" && (pDiff < -5) && hedgerStatus == "open") {
                tradeStatusReWrite('1', currentPrice, 'miss', priceMoveDiff, tradePrice);
                // const tradeBuy = require("./tradeBuy")
                // tradeBuy(currentPrice);
                console.log('MISS SELL [Hold AT LIMIT]');
            }

            //? LOWER AND UPPER PANIC 
            //? Takes Sell or Buy Action on Hit and Miss
            //  Hit buy of Dispare
            else if (tradeStatusTrade == "0" && (pDiff < -40) && hedgerStatus == "hit") {
                //TODO Replace with DAILY LOSS LIMIT ^
                tradeStatusReWrite('1', currentPrice, 'open', priceMoveDiff, currentPrice);
                const tradeBuy = require("./tradeBuy")
                tradeBuy(currentPrice);
                console.log('HIT BUY [STOP LOSS]');
            }
            // Sell stop loss
            else if (tradeStatusTrade == "0" && (pDiff > 100) && hedgerStatus == "miss") {
                //TODO Replace with DAILY LOSS LIMIT ^
                tradeStatusReWrite('1', currentPrice, 'miss', priceMoveDiff, tradePrice);
                // const tradeSell = require("./tradeSell")
                // tradeSell(currentPrice)
                console.log('MISS SELL [STOP LOSS]');
            }

            else if (tradeStatusTrade == "1" && (pDiff > 100) && hedgerStatus == "hit") {
                //TODO Replace with DAILY LOSS LIMIT ^
                tradeStatusReWrite('0', currentPrice, 'open', priceMoveDiff, currentPrice);
                const tradeSell = require("./tradeSell")
                tradeSell(currentPrice)
                console.log('HIT SELL [STOP LOSS]');
            }
            else if (tradeStatusTrade == "1" && (pDiff < -86) && hedgerStatus == "miss") {
                //TODO Replace with DAILY LOSS LIMIT ^
                tradeStatusReWrite('0', currentPrice, 'open', priceMoveDiff, currentPrice);
                const tradeBuy = require("./tradeBuy")
                tradeBuy(currentPrice);
                console.log('MISS BUY [STOP LOSS]');
            }

            //! Adjustment of pCurrwrite to tradePrice
            //? Reset Actions
            else if (tradeStatusTrade == "0" && (pDiff > 0) && hedgerStatus == "hit") {
                tradeStatusReWrite('0', tradePrice, 'open', priceMoveDiff, tradePrice);
                console.log('Params Reached [hit] -- Bot Action ReEngage');
            }

            else if (tradeStatusTrade == "0" && (pDiff < 4) && hedgerStatus == "miss") {
                tradeStatusReWrite('0', tradePrice, 'open', priceMoveDiff, tradePrice);
                console.log(' Params Reached [miss] -- Bot Action ReEngage');
            }

            else if (tradeStatusTrade == "1" && (pDiff < 0) && hedgerStatus == "hit") {
                tradeStatusReWrite('1', tradePrice, 'open', priceMoveDiff, tradePrice);
                console.log('Params Reached [hit] -- Bot Action ReEngage');
            }

            else if (tradeStatusTrade == "1" && (pDiff > -4) && hedgerStatus == "miss") {
                tradeStatusReWrite('1', tradePrice, 'open', priceMoveDiff, tradePrice);
                console.log('Params Reached [miss]-- Bot Action ReEngage');
            }

            else {
                hedgeMoveReWrite(priceMoveDiff, currentPrice)
            }
        }


        async.parallel([
            function (callback) {
                setTimeout(function () {
                    getTimeHedger();
                    callback(null, 1);
                }, 0);
            },
            function (callback) {
                setTimeout(function () {
                    getCurrentPrice()
                    callback(null, 1);
                }, 0);
            },
            function (callback) {
                setTimeout(function () {
                    getPriceTrade()
                    callback(null, 1);
                }, 600);
            },

            function (callback) {
                setTimeout(function () {
                    getPriceMove()
                    callback(null, 1);
                }, 800);
            },
            function (callback) {
                setTimeout(function () {
                    getTradeStatusPrev()
                    callback(null, 1);
                }, 1000);
            },
            function (callback) {
                setTimeout(function () {
                    makePriceMove(prevPrice, currentPrice)
                    callback(null, 1);
                }, 1300);
            },
            function (callback) {
                setTimeout(function () {
                    tradeStatus(tradeStatusTrade, currentPrice, tradePrice, priceMoveCurrent)
                    callback(null, 1);
                }, 1500);
            }
        ])
    })
}
// hedgerBNB()
module.exports = hedgerBNB;