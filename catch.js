const cron = require('node-cron');
const Binance = require('binance-api-node').default
require('dotenv').config();;
const fs = require('fs')
var async = require('async');
var await = require('await');



//? Authenticate Binance API
const client = Binance({
    apiKey: process.env.BNB_API_KEY,
    apiSecret: process.env.BNB_API_SECRET,
    // getTime: xxx // time generator function, optional, defaults to () => Date.now()
})

function Catch() {
    cron.schedule('0,5,10,30,35,40,45,50,55 * * * *', () => {
        async function catchFunc() {
            //? Get Current TradeStatus
            let tradeStatusData = "";
            fs.readFile('./dataRun/dbTradeStatus.json', function (err, data) {
                let json = JSON.parse(data)
                tradeStatusData = json.tradeStatus;
            })

            //? Get account info from Binanace API
            let data = await client.accountInfo()
            let data0 = data.balances;
            let data1 = data.balances;

            //? Filter BTC values
            let acntBtc0 = data0.filter(it => it.asset.includes('BTC'));
            let acntBtc = acntBtc0[0];

            //? Filter USDT values
            let acntUsdt0 = data1.filter(it => it.asset.includes('USDT'));
            let acntUsdt = acntUsdt0[0];

            //! CATCH Logic 
            if (acntUsdt.free < 50 && tradeStatusData == '0') {
                console.log('⬆ CATCH ⬆ USDT ⬆ MARKET SELL ⬆')

                //? Cancel Outstanding Order
                const tradeCancel = require('./tradeCancel')
                tradeCancel()

                //? Sell
                const tradeSellMarket = require("./tradeSellMarket")
                tradeSellMarket();

            } else if (acntUsdt.free > 50 && tradeStatusData == '1') {
                console.log('⬆ CATCH ⬆ BTC ⬆ MARKET BUY ⬆')

                //? Cancel Outstanding Order
                const tradeCancel = require('./tradeCancel')
                tradeCancel()

                //? Buy
                const tradeBuyMarket = require("./tradeBuyMarket")
                tradeBuyMarket();
            }
            else {
                console.log('CATCH ✔')
            }
        }
        catchFunc();
    })
}
// Catch()
module.exports = Catch;