#!./usr/bin/python3
import sys, json
import gpt_2_simple as gpt2

#Read data from stdin
def read_in():
    lines = sys.stdin.readlines()
    # Since our input would only be having one line, parse our JSON data from that
    return json.loads(lines[0])

def ml_func():
    lines = read_in()
    binary_input = str(lines)
    sess = gpt2.start_tf_sess()
    gpt2.load_gpt2(sess)

    gpt2.generate(sess,
                  length=20,
                  temperature=0.7,
                  prefix=binary_input,
                  nsamples=1
                  )

def main():
    #get our data as an array from read_in()
    run = ml_func()

    # Sum  of all the items in the providen array
    # total_sum_inArray = 0
    # for item in lines:
    #     total_sum_inArray += item
    print (run)

# Start process
if __name__ == '__main__':
    main()

# # script.py
# binaryInput = 1010111010111010101110100110011001100010101100101111001


# print ("%d" % binaryInput)
# # print "He's %d centimeters tall." % my_height
# # print "He's %d kilograms heavy." % my_weight
# # print "Actually that's not too heavy."
# # print "He's got %s eyes and %s hair." % (my_eyes, my_hair)
# # print "His teeth are usually %s depending on the coffee." % my_teeth

# # # this line is tricky, try to get it exactly right
# # print "If I add %d, %d, and %d I get %d. I don't know what that means but, whatever." % (
# #     my_age, my_height, my_weight, my_age + my_height + my_weight)