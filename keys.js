require('dotenv').config();

module.exports = {
    TradeQuantity: .007,
    BNBApiKey: process.env.BNB_API_KEY,
    BNBApiSecret: process.env.BNB_API_SECRET
}