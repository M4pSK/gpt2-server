const cron = require('node-cron');
const Binance = require('binance-api-node').default
require('dotenv').config();;
const fs = require('fs')
var async = require('async');
var await = require('await');

//? Authenticate Binance API
const client = Binance({
    apiKey: process.env.BNB_API_KEY,
    apiSecret: process.env.BNB_API_SECRET,
    // getTime: xxx // time generator function, optional, defaults to () => Date.now()
})

let allIn = 0;
function wager(exchangeAction) {
    // cron.schedule('21,36,46,56,6 * * * *', () => {
    async function wagerFunc() {
        let getPrice = await client.avgPrice({ symbol: 'BTCUSDT' });
        let btcPrice = getPrice.price
        console.log(`BTC Price: ${btcPrice}`)

        //? Get account info from Binanace API
        let data = await client.accountInfo()
        let data0 = data.balances;
        let data1 = data.balances;
        //? Filter BTC values
        let acntBtc0 = data0.filter(it => it.asset.includes('BTC'));
        let acntBtc = acntBtc0[0];
        let balBtc = acntBtc.free
        console.log(`Bal BTC: ${balBtc}`)

        //? Filter USDT values
        let acntUsdt0 = data1.filter(it => it.asset.includes('USDT'));
        let acntUsdt = acntUsdt0[0];
        let balUsdt = acntUsdt.free
        console.log(`Bal USDT: ${balUsdt}`)

        if (exchangeAction == "sell") {
            allIn = balBtc
            console.log(allIn.toFixed(3));
            return (allIn.toFixed(3));
        }
        if (exchangeAction == "buy") {
            allIn = balUsdt / btcPrice
            console.log(allIn.toFixed(3));
            return (allIn.toFixed(3));
        }
    }
    wagerFunc();
    // })
}
module.exports = wager;