const async = require('async');
var await = require('await');
const Binance = require('binance-api-node').default
require('dotenv').config();;
const axios = require('axios')
const keys = require('./keys')
const wager = require('./wager100')

//? Authenticate Binance API
const client = Binance({
    apiKey: process.env.BNB_API_KEY,
    apiSecret: process.env.BNB_API_SECRET,
    // getTime: xxx // time generator function, optional, defaults to () => Date.now()
})

async function TradeBuy() {
    let getPrice = await client.avgPrice({ symbol: 'BTCUSDT' });
    let btcPrice0 = getPrice.price
    let btcPrice = Number(btcPrice0).toFixed(2)
    console.log(`BTC Price @ Trade: ${btcPrice}`)

    //? Get account info from Binanace API
    let data = await client.accountInfo()
    let data0 = data.balances;
    let data1 = data.balances;
    //? Filter BTC values
    let acntBtc0 = data0.filter(it => it.asset.includes('BTC'));
    let acntBtc = acntBtc0[0];
    let balBtc = acntBtc.free
    console.log(`Bal BTC: ${balBtc}`)

    //? Filter USDT values
    let acntUsdt0 = data1.filter(it => it.asset.includes('USDT'));
    let acntUsdt = acntUsdt0[0];
    let balUsdt = acntUsdt.free
    console.log(`Bal USDT: ${balUsdt}`)
    let usdtToBtc = balUsdt / btcPrice
    let bufferAmnt = usdtToBtc - (usdtToBtc / 10)
    let allIn = bufferAmnt.toFixed(4)

    try {
        console.log(`Placing Order of: ${allIn}BTC At: ${btcPrice}BTC`);
        await client.order({
            symbol: 'BTCUSDT',
            side: 'BUY',
            quantity: allIn,
            type: 'LIMIT',
            timeInForce: 'GTC',
            price: btcPrice
        })
        console.log(`
...............'',;;;;;;,'..............
..........'';cldxkOOOOkkkkxdl:'.........
........,:cloddolc:,,''',;::lool;.......
.....'clodkkd:'............'''',cc;.....
....cdxxkxo;................'''...;c'...
...okkxdc'....................''...,l;..
..oOkd:'........................''..:d,.
.:OOl'.......---.................''.'xd.
.d0c.............................'''dk,
.x0c...............BUY............',;xO;
.x0c..............................',lOO;
.o0:................1.............,ck0x'
.;xc..............................:kOOc.
..:c;'............................lOOl..
...,;;,..........................'x0l...
....,:;;'........... ............dkc....
......,:c,.....................;ol'.....
........,:lllc,...........'',;::'.......
...........;codoollccc:::;;;,'..........
................';;;;,,'...  .........::`)
    }
    catch (error) {
        console.log('error')
        throw error
    }
}
// TradeBuy()
module.exports = TradeBuy;