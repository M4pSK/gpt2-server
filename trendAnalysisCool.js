//**************NOTE****************//
//* Purpose: ReActivate Bot After Cool period
//* Action : will toggle hedgerStatus to "open" if majority of last 5 trades are 1s  
//* dbTradeStatus.json
//*********************************//

const async = require('async');
const await = require('await');
const cron = require('node-cron');
const fs = require('fs');

function trendAnalysisCool() {
    //* Explicitly written
    cron.schedule('19 * * * *', () => {

        //! NOTE:
        //? Change focusResolution to adjust hours
        let focusResolution = 5

        let binaryhistLog = '';
        //? Read binary Data
        async function readTradeBinaryHist() {
            console.log(`//********Trend Analysis*********//`);
            console.log(`//*************Cool*************//`);
            fs.readFile('./dataRun/dbBinary0.json', function (err, data) {
                let jsonDbBinary = JSON.parse(data)
                binaryhistLog = JSON.stringify(jsonDbBinary.binaryLog);
            })
        }

        let tradeStatusCur = '';
        let hedgeStatusCur = '';
        //? Read tradeStatus Data
        async function readTradeStatusHist() {
            fs.readFile('./dataRun/dbTradeStatus.json', function (err, data) {
                let json = JSON.parse(data)
                let json0 = JSON.parse(data)
                tradeStatusCur = json.tradeStatus;
                hedgeStatusCur = json0.hedgerStatus;
            })
        }

        async function tradeHistReview(binaryLogData) {
            //* Remove " "double quotes" 
            let BLD0 = binaryLogData.replace(/['"]+/g, '')
            let binaryhistFocus = BLD0.substr(BLD0.length - focusResolution);
            console.log(`binaryhistFocus: ${binaryhistFocus}`)

            //* Extract 1 & 0 count
            let binaryCount0 = (binaryhistFocus.match(/0/g) || []).length;
            let binaryCount1 = (binaryhistFocus.match(/1/g) || []).length;
            console.log(`binaryCount0: ${binaryCount0}`);
            console.log(`binaryCount1: ${binaryCount1}`);

            if (binaryCount0 < binaryCount1 && (tradeStatusCur == "0") && (hedgeStatusCur == "cool")) {
                console.log(`Trend Analysis Complete tradeStatus = open`);
                //! Action 
                let dbBinaryReactivateJson = { "tradeStatus": "0", "hedgerStatus": "open" }
                fs.writeFileSync('./dataRun/dbTradeStatus.json', JSON.stringify(dbBinaryReactivateJson), 'utf-8')
            }
            else {
                console.log(`Market Trend Down // Keep on Cool`);
            }
        }

        async.parallel([
            function (callback) {
                setTimeout(function () {
                    readTradeBinaryHist(); //function to run
                    callback(null,
                        1);
                }, 0); //time till run
            },
            function (callback) {
                setTimeout(function () {
                    readTradeStatusHist(); //function to run
                    callback(null,
                        1);
                }, 0); //time till run
            },
            function (callback) {
                setTimeout(function () {
                    tradeHistReview(binaryhistLog);
                    callback(null,
                        1);
                }, 600);
            },
        ])
    }
    )
}
// trendAnalysisCool()
module.exports = trendAnalysisCool