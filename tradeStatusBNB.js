var async = require('async');
const fs = require('fs');

//* Extract previous and current trades
const tradeStatusBNB = (tradeStatusCur, GPT2Pred) => {
    // ! Error in parsing response: adding +42 for ubuntu
    let tradeStatusCompare = tradeStatusCur + GPT2Pred;
    console.log('Trade Status Compare: ' + tradeStatusCompare)
    if (tradeStatusCompare == 10) {
        //* Buy
        const TradeBuy = require("./tradeBuy")
        TradeBuy();
        console.log("Trade Status: BUY")
    }
    else if (tradeStatusCompare == 01) {
        //* Sell 
        const TradeSell = require("./tradeSell")
        TradeSell();
        console.log("Trade Status: SELL")
    }
    else {
        console.log("Trade Status: hold")
    }
}
module.exports = tradeStatusBNB;