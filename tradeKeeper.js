const fs = require('fs');
//? Logs tradeKeeper dbTK
//? Price since Previous Trade
//* Note: Prevents Low price rate runaway 
//* Will only be caught by Hedger

function tradeKeeper(tsPrev, tsCur, tsPricePrev, tsPriceCur) {
    if (tsPrev == tsCur) {
        let jsonDbTK = { "tradeKeeper": tsPricePrev }
        fs.writeFileSync('./dataRun/dbTradeKeeper.json', JSON.stringify(jsonDbTK), 'utf-8')

    } else if (tsPrev !== tsCur) {
        let jsonDbTK0 = { "tradeKeeper": tsPriceCur }
        fs.writeFileSync('./dataRun/dbTradeKeeper.json', JSON.stringify(jsonDbTK0), 'utf-8')
    }
}

module.exports = tradeKeeper;