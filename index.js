const active = require("./active");
const hedger = require("./hedger");
const Catch = require("./catch")
// const forever = require("./pm2")
const faunaLogger = require("./faunaLogger")

const trendAnalysisCool = require('./trendAnalysisCool');

active();
hedger();
Catch();
trendAnalysisCool();
// forever();

faunaLogger();