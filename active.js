const cron = require('node-cron');

function active() {
    cron.schedule('20 * * * *', () => {
        // cron.schedule('20 * 2,4,6,8,10,12,14,16,18,20,22,23,24,26,28,30 * *', () => {
        const activeRuntime = require("./activeRuntime")
        activeRuntime();
    })
};

module.exports = active;