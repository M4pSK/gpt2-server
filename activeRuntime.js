var async = require('async');
var await = require('await');
const fs = require('fs');

const Binance = require('binance-api-node').default
require('dotenv').config();
const Moment = require('moment');

//? Initiate Active Runtime
//* 'node activeRuntime.js' to run without cron timer
function activeRuntime() {
    //? Authenticate Binance API
    const client = Binance()
    // Authenticated client, can make signed calls
    const client2 = Binance({
        apiKey: process.env.BNB_API_KEY,
        apiSecret: process.env.BNB_API_SECRET,
        // getTime: xxx // time generator function, optional, defaults to () => Date.now()
    })

    console.log(' ---------------------------------------- ACTIVE TRADING Binance ----------------------------------------')

    //? Get Time
    //* read from Moment
    async function getTime() {
        //? Timestamp
        let timeStamp = Moment().format('lll');
        let unixStamp = Moment().unix();
        console.log('Time Stamp: ' + timeStamp)
        console.log('Unix Stamp: ' + unixStamp)
    }

    //? Get Previous Position
    //* read from dbTradeStatus.json
    let tradeStatusPrev = "";
    let hedgerData = "";
    // let futureSellData = false;
    async function getPrevTradeStatus() {
        fs.readFile('./dataRun/dbTradeStatus.json', function (err, data) {
            let json = JSON.parse(data)
            let json0 = JSON.parse(data)
            // let json1 = JSON.parse(data)

            let tsData = json.tradeStatus;
            let hsData = json0.hedgerStatus;
            // let fsData = json1.futureSellData;

            tradeStatusPrev = tsData;
            hedgerData = hsData;
            // futureSellData = fsData;
            console.log('TradeStatus Previous: ' + tradeStatusPrev);
            console.log('HedgerStatus: ' + hedgerData);
            // console.log('futureSellData: ' + futureSellData);
        })
    }

    //?Get Previous Price
    let previousPrice = 0;
    async function getPrevPrice() {
        fs.readFile('./dataRun/dbRun0.json', function (err, data) {
            var json = JSON.parse(data)
            previousPrice = json.tradePrice;
            console.log('Previous BTC Price: ' + previousPrice);
        })
    }

    //? Extract Price at previous trade
    let tradePriceKeep = "";
    async function getPriceTradeKeep() {
        fs.readFile('./dataRun/dbTradeKeeper.json', function (err, data) {
            let json = JSON.parse(data)
            tradePriceKeep = json.tradeKeeper;
            console.log('Price at Trade: ' + tradePriceKeep);
        })
    }

    //?Get Crypto Price
    //* using Binance API */
    let currentPrice = 0;
    async function getCurrentPrice() {
        let getPrice = await client.avgPrice({ symbol: 'BTCUSDT' });
        // console.log(getPrice)
        currentPrice = getPrice.price;
        console.log('Current BTC Price: ' + currentPrice);
    }

    //? Get Position + Current Crypto Price
    //? MAKE BINARY FOR LOG NOT FOR TRADING
    let binaryCur = 0;
    async function makeBinary() {
        //!//** Binary Logic */
        if (previousPrice < currentPrice) {
            binaryCur = 1;
        }
        else if (previousPrice > currentPrice) {
            binaryCur = 0;
        }
        else {
            binaryCur = 1;
        }
        console.log('Binary Current: ' + binaryCur);
    };

    //? Logs TradePrice dbRun0
    //? Price since Previous hour
    //* Note: Does not effect the tradekeeper price
    //* for datalog
    async function currentPriceDbPush(priceCur) {
        let jsonDbReWrite = { "tradePrice": priceCur }
        fs.writeFileSync('./dataRun/dbRun0.json', JSON.stringify(jsonDbReWrite), 'utf-8')
    }

    //? Logs previous tradeprice
    //* Only runs on cooldown
    //* Ref tradePriceKeep & getPriceTradeKeep()  
    //* Logs to TradeKeeperDB.json
    async function previousTradeKeepDbPush(tradeKeepPrev) {
        let jsonDbReWrite = { "tradeKeeper": tradeKeepPrev }
        fs.writeFileSync('./dataRun/dbTradeKeeper.json', JSON.stringify(jsonDbReWrite), 'utf-8')
    }

    let binaryLog = 0;
    let binaryInput = '';
    let GPT2Predict = '';
    let binaryLogRaw = '';
    let inputLen = 0;
    async function binaryDbPush(prevAffirmation) {
        //? Read binary Data
        fs.readFile('./dataRun/dbBinary0.json', function (err, data) {
            var jsonDbBinary = JSON.parse(data)
            binaryLogRaw = JSON.stringify(jsonDbBinary.binaryLog);

            //? Replace odd characters from Python Transfer
            let bL = binaryLogRaw.replace(/,/g, "");
            let bL0 = bL.replace('[', '');
            let bL1 = bL0.replace(']', '');
            let bL2 = bL1.replace(/['"]+/g, '');
            binaryLog = bL2.concat(binaryCur.toString());

            //? Keep Binary Input in Zone (800-890)
            //* 
            let binaryLogTreat;
            let trimInputLen = binaryLog.length;
            console.log(`Input Length Check: ${trimInputLen}`);
            if (trimInputLen >= 890) {
                console.log('Trimming Input Length Back to 800');
                let blZone = binaryLog.substring(100)
                binaryLogTreat = blZone

            } else {
                binaryLogTreat = binaryLog;
                console.log(`Input will readjust in ${890 - trimInputLen} hours`)
            }
            // console.log(binaryLogTreat);

            //? Bot Affirmation
            let binaryLogAffirmation = prevAffirmation + binaryLogTreat;
            console.log(`Affirmation: ${binaryLogAffirmation}`)


            binaryInput = JSON.stringify(binaryLogAffirmation);
            inputLen = binaryInput.length;

            let binaryInputJsonWrite = { "binaryLog": binaryLogTreat }
            fs.writeFileSync('./dataRun/dbBinary0.json', JSON.stringify(binaryInputJsonWrite), 'utf-8')
            GPT2Input = JSON.stringify(binaryInputJsonWrite);
        })
    }

    async.parallel([
        function (callback) {
            setTimeout(function () {
                getTime();
                callback(null, 1);
            }, 0);
        },
        function (callback) {
            setTimeout(function () {
                getPrevPrice();
                callback(null, 1);
            }, 0);
        },
        function (callback) {
            setTimeout(function () {
                getPriceTradeKeep();
                callback(null, 1);
            }, 0);
        },
        function (callback) {
            setTimeout(function () {
                getCurrentPrice();
                callback(null, 1);
            }, 2000);
        },
        function (callback) {
            setTimeout(function () {
                getPrevTradeStatus();
                callback(null, 1);
            }, 6000);
        },
        function (callback) {
            setTimeout(function () {
                makeBinary();
                callback(null, 1);
            }, 7000);
        },
        function (callback) {
            setTimeout(function () {
                binaryDbPush(tradeStatusPrev);
                callback(null, 1);
            }, 9000);
        },
        function (callback) {
            setTimeout(function () {
                currentPriceDbPush(currentPrice);
                callback(null, 1);
            }, 10000);
        },
        function (callback) {
            setTimeout(function () {
                if (hedgerData == 'open') {
                    const CryptoBot = require("./cryptoBot.js")
                    CryptoBot(tradeStatusPrev, binaryInput, tradePriceKeep, currentPrice);
                } else {
                    previousTradeKeepDbPush(tradePriceKeep)
                    console.log('BOT ON COOLDOWN')
                }
                callback(null, 1);
            }, 10200);
        }
    ])
};

//! Bot Dev Mode
//? Set with node activeRuntime.js -dev
// if (process.argv[2] === '-dev') {
//     console.log(`
//     .......:............®®®.........:......

//     .....:....®.®®®®®®®®..............:....

//     ...:........®®.®®®®®................:..

//     .-...........®®®®..®..................-

//     ...........®®®®.®..®...................

//     .:.........®®.®®..®®..................:

//     ..:........®®...®®..®.®®®®...........:.

//     ....:......®®.®®....®®®®®®®®.......:...`);
//     activeRuntime()
// } else {
//     activeRuntime()
//     // console.log('');
// }
// activeRuntime()
module.exports = activeRuntime;