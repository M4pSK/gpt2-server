var async = require('async');
var await = require('await');
const { JsonDB } = require('node-json-db');
require('dotenv').config();
const { Config } = require('node-json-db/dist/lib/JsonDBConfig')
const cron = require('node-cron');
const Binance = require('binance-api-node').default
const dbTradeStatus = require('./dataRun/dbTradeStatus.json');
const dbRun0 = require('./dataRun/dbRun0.json');
const fs = require('fs');
const Moment = require('moment');

const tradeSell = require("./tradeSell")
const tradeBuy = require("./tradeBuy")


function hedger() {
    //* Explicitly written
    cron.schedule('0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59 * * * *', () => {
        //? Authenticate Binance API
        const client = Binance()
        // Authenticated client, can make signed calls
        const client2 = Binance({
            apiKey: process.env.BNB_API_KEY,
            apiSecret: process.env.BNB_API_SECRET,
            // getTime: xxx // time generator function, optional, defaults to () => Date.now()
        })
        // client.time().then(time => console.log(time))

        //? Get Time
        //* read from dbTradeStatus.json
        async function getTimeHedger() {
            console.log('-------Hedger-------')
            //? Timestamp
            let timeStamp = Moment().format('lll');
            let unixStamp = Moment().unix();
            console.log('Time Stamp: ' + timeStamp)
            // console.log('Unix Stamp: ' + unixStamp)
        }

        //? Get TradeStatus + Price @ Trade
        let tradeStatusTrade = "";
        let hedgerStatus = "";
        async function getTradeStatusPrev() {
            fs.readFile('./dataRun/dbTradeStatus.json', function (err, data) {
                let json = JSON.parse(data)
                let json0 = JSON.parse(data)
                tradeStatusTrade = json.tradeStatus;
                hedgerStatus = json0.hedgerStatus;
            })
        }

        let tradePrice = "";
        //* Extract Price at previous trade
        async function getPriceTrade() {
            fs.readFile('./dataRun/dbTradeKeeper.json', function (err, data) {
                let json = JSON.parse(data)
                tradePrice = json.tradeKeeper;
            })
        }

        //? Get Previous Hedger Price
        //? Get Price Movement
        let priceMovePrevious = 0;
        let prevPrice = 0;
        async function getPriceMove() {
            fs.readFile('./dataRun/dbPriceMove.json', function (err, data) {
                let json = JSON.parse(data)
                let json0 = JSON.parse(data)
                priceMovePrevious = json.moveDiff;
                prevPrice = json0.hedgePrice
                console.log('Prev logged diff: ' + priceMovePrevious);
            })
        }

        let currentPrice = 0;
        async function getCurrentPrice() {
            let getPrice = await client.avgPrice({ symbol: 'BTCUSDT' });
            currentPrice = getPrice.price;
        }

        let priceMoveCurrent = 0;
        async function makePriceMove(pricePrev, PriceCurr) {
            let priceMoveCurrent = pricePrev - PriceCurr;
            console.log('Price Move Current: ' + priceMoveCurrent)
        }

        async function tradeStatus(tradeSts, prevTradeP, curTradeP) {
            async function tradeStatusReWrite(tReWrite, pReWrite, hedgerStsWrite, pMoveDiffWrite, pMoveCurrWirte) {
                let jsonTradeStatusReWrite = { "tradeStatus": tReWrite, "hedgerStatus": hedgerStsWrite }
                fs.writeFile("./dataRun/dbTradeStatus.json", JSON.stringify(jsonTradeStatusReWrite), (error) => {
                })

                let jsonPRiceReWrite = { "tradeKeeper": pReWrite }
                fs.writeFile("./dataRun/dbTradeKeeper.json", JSON.stringify(jsonPRiceReWrite), (error) => {
                })

                let jsonMoveDiffReWrite = { "moveDiff": pMoveDiffWrite, "hedgePrice": pReWrite }
                fs.writeFile("./dataRun/dbPriceMove.json", JSON.stringify(jsonMoveDiffReWrite), (error) => {
                })
            }

            async function hedgeMoveReWrite(pMoveDiff, pCurrentMove) {
                let jsonHedgerMoveReWrite = { "moveDiff": pMoveDiff, "hedgePrice": pCurrentMove }
                fs.writeFile("./dataRun/dbPriceMove.json", JSON.stringify(jsonHedgerMoveReWrite), (error) => {
                })
            }

            console.log('TradeStatus: ' + tradeStatusTrade)
            console.log('HedgerStatus: ' + hedgerStatus)
            console.log('Trade Price: ' + (Math.round(tradePrice * 100) / 100).toFixed(2))
            console.log('Hedger Price: ' + (Math.round(currentPrice * 100) / 100).toFixed(2))
            let pDiff = currentPrice - tradePrice

            //? IMPORTANT / CONFUSING: Takes the difference of the difference to de bounce API errors
            let priceMoveDiff = currentPrice - prevPrice

            console.log('Price Move: ' + (Math.round(priceMoveDiff * 100) / 100).toFixed(2))
            console.log('Price Diff: ' + (Math.round(pDiff * 100) / 100).toFixed(2))

            //? Catches price if it falls below a certain margin
            //? Regular Hourly Crypto Bot Runtime Strategy
            //? Check if Price Error
            if (currentPrice == 0) {
                console.log('Binance Price API Error: 0')
            }
            //? Debounces difference between price increase
            //* (Lower bounds deBounce)(Upper bounds deBounce)
            // else if ((priceMoveDiff > 33) || (priceMoveDiff < -33)) {
            //     console.log('API MISSFIRE 1 MIN VALUE OVER 40')
            // }

            //!------------------------------------------------------------------------
            //! STATES
            // Regular Runtime
            //? 0. open, 0, -40 ➡ 1, hit
            //? 1. open, 1, +40 ➡ 0, hit
            //? 2. hit, 0, +0 ➡ 0, open*
            //? 3. hit, 1, -0 ➡ 1, open
            //  Miss Runtime
            //? 4. open, 0, +15 ➡ 0, miss 
            //? 5. open, 1, -5  ➡ 1, miss
            //? 6. miss, 0, +4 ➡ 1, open
            // Miss ReEngage 
            //? 7. miss, 1, +0 ➡ 0, open
            // Miss Lower Limit
            //? 8. miss, 1, -38 ➡ 1, lowMiss   -- EXPERIMENT DISABLED 1 / 22 / 2020
            // Miss Lower Limit ReEngage
            //? 9. lowMiss, 1, +0 ➡ 0, open    -- EXPERIMENT DISABLED 1 / 22 / 2020
            //* - tradeSell() into stair climb   -- EXPERIMENT DISABLED 1 / 22 / 2020
            // Stop Loss
            //? 10. lowMiss, 1, -65 ➡ 0, miss
            //* cool
            // Stair Climb
            //? 11. hit, 0, +20 ➡ 0, open
            // Reverse Dispare
            //? 12. hit, 1, -80 ➡ 0, miss[cool] 
            // Notes
            // * Optional for buy-in [test: gain-loss analysis]

            // Regular Runtime
            //? 0. 0, Open, -40 ➡ 1, hit, buy
            //* Action: tradeBuy 
            else if (tradeStatusTrade == "0" && (pDiff < -40) && hedgerStatus == "open") {
                tradeStatusReWrite('1', currentPrice, 'hit', priceMoveDiff, currentPrice);
                ;
                tradeBuy(currentPrice);
                console.log('0, Open, -40 ➡ 1, hit, buy');
            }
            //? 1. 1, open, +40 ➡ 0, hit, sell
            //* Action: tradeSell
            else if (tradeStatusTrade == "1" && (pDiff > 40) && hedgerStatus == "open") {
                tradeStatusReWrite('0', currentPrice, 'hit', priceMoveDiff, currentPrice);
                tradeSell(currentPrice)
                console.log('1, open, +40 ➡ 0, hit, sell');
            }
            //? 2. 0, hit, +0 ➡ 0, open
            //* Action: ReEngage Bot
            else if (tradeStatusTrade == "0" && (pDiff < 0) && hedgerStatus == "hit") {
                tradeStatusReWrite('0', tradePrice, 'open', priceMoveDiff, tradePrice);
                console.log('0, hit, +0 ➡ 0, open');
            }
            //? 3. 1, hit, -0 ➡ 0, open
            //* Action: ReEngage Bot
            else if (tradeStatusTrade == "1" && (pDiff > 0) && hedgerStatus == "hit") {
                tradeStatusReWrite('1', tradePrice, 'open', priceMoveDiff, tradePrice);
                console.log('1, hit, -0 ➡ 0, open');
            }

            //!------------------------------------
            // Miss Runtime
            //? 4. 0, open, +15 ➡ 0, miss
            //* Action: DisEngage Bot 
            // Optional on BUY
            //! ************ Experiment ************ 
            //! Time: dec/27/10:24p
            //! Note: Disabled buy in block
            //   tradeStatusReWrite('0', tradePrice, 'miss', priceMoveDiff, tradePrice);
            else if (tradeStatusTrade == "0" && (pDiff > 15) && hedgerStatus == "open") {
                tradeStatusReWrite('0', tradePrice, 'open', priceMoveDiff, tradePrice);
                console.log('0, open, +15 ➡ 0, open');
                console.log('Note: Disabled buy in block');

            }
            //? 5. 1, open, -10 ➡ 1, miss
            //* Action: DisEngage Bot
            else if (tradeStatusTrade == "1" && (pDiff < -10) && hedgerStatus == "open") {
                tradeStatusReWrite('1', tradePrice, 'miss', priceMoveDiff, tradePrice);
                console.log('1, open, -10 ➡ 1, miss');
            }
            //? 6. 0, miss, +4 ➡ 0, open
            //* Action: ReEngage Bot
            else if (tradeStatusTrade == "0" && (pDiff < -4) && hedgerStatus == "miss") {
                tradeStatusReWrite('0', tradePrice, 'open', priceMoveDiff, tradePrice);
                console.log(' 0, miss, +4 ➡ 0, open');
            }
            //? 7. 1, miss, +0 ➡ 1, open
            //! LATE CHANGE DAY 2 
            else if (tradeStatusTrade == "1" && (pDiff > 4) && hedgerStatus == "miss") {
                tradeStatusReWrite('1', tradePrice, 'open', priceMoveDiff, tradePrice);
                console.log('1, miss, +0 ➡ 1, open');
            }
            // Miss Lower Limit
            // //? 8. miss, 1, -38 ➡ 1, lowMiss   -- EXPERIMENT DISABLED 1 / 22 / 2020
            // else if (tradeStatusTrade == "1" && (pDiff < -38) && hedgerStatus == "miss") {
            // tradeStatusReWrite('1', tradePrice, 'lowMiss', priceMoveDiff, tradePrice);
            // console.log('miss, 1, -38 ➡ 1, lowMiss');
            // }
            // Miss Lower Limit ReEngage
            //? 9. lowMiss, 1, +0 ➡ 0, open   -- EXPERIMENT DISABLED 1 / 22 / 2020
            //* Action: [tradeSell]   -- EXPERIMENT DISABLED 1 / 22 / 2020
            // else if (tradeStatusTrade == "1" && (pDiff > 0) && hedgerStatus == "lowMiss") {
            // tradeStatusReWrite('0', tradePrice, 'open', priceMoveDiff, tradePrice);
            // tradeSell(currentPrice)
            // console.log('lowMiss, 1, +0 ➡ 0, open');
            // }

            // Stop Loss
            //? 10. miss, 1, -65 ➡ 0, miss[cool]
            //* NOTE: Will be ReEngaged by trendAnalysisCool.js
            else if (tradeStatusTrade == "1" && (pDiff < -65) && hedgerStatus == "miss") {
                tradeStatusReWrite('0', tradePrice, 'cool', priceMoveDiff, tradePrice);
                tradeSell(currentPrice)
                console.log('1, miss, -65 ➡ 0 Cool [StopLoss]');
            }
            //!------------------------------------
            // Stair Climb
            //? 11. 0, hit, +20 ➡ 1 open
            else if (tradeStatusTrade == "0" && (pDiff > 20) && hedgerStatus == "hit") {
                tradeStatusReWrite('1', tradePrice, 'open', priceMoveDiff, tradePrice); 2
                tradeBuy(currentPrice)
                console.log('0, hit, +20 ➡ 1 open [Stair Climber ++]');
            }
            // Reverse Dispare
            //? 12. 1, hit, -80 ➡ 0 miss[cool]
            else if (tradeStatusTrade == "1" && (pDiff < -80) && hedgerStatus == "hit") {
                tradeStatusReWrite('0', tradePrice, 'cool', priceMoveDiff, tradePrice);
                tradeSell(currentPrice)
                console.log('1, hit, -100 ➡ 0 cool [Reverse Dispare]');
            }

            else {
                hedgeMoveReWrite(priceMoveDiff, currentPrice)
                console.log("Hedger Update");
            }
        }


        async.parallel([
            function (callback) {
                setTimeout(function () {
                    getTimeHedger();
                    callback(null, 1);
                }, 0);
            },
            function (callback) {
                setTimeout(function () {
                    getCurrentPrice()
                    callback(null, 1);
                }, 400);
            },
            function (callback) {
                setTimeout(function () {
                    getPriceTrade()
                    callback(null, 1);
                }, 600);
            },

            function (callback) {
                setTimeout(function () {
                    getPriceMove()
                    callback(null, 1);
                }, 800);
            },
            function (callback) {
                setTimeout(function () {
                    getTradeStatusPrev()
                    callback(null, 1);
                }, 1000);
            },
            function (callback) {
                setTimeout(function () {
                    makePriceMove(prevPrice, currentPrice)
                    callback(null, 1);
                }, 1300);
            },
            function (callback) {
                setTimeout(function () {
                    tradeStatus(tradeStatusTrade, currentPrice, tradePrice, priceMoveCurrent)
                    callback(null, 1);
                }, 1500);
            }
        ])
    })
}
// hedger()
module.exports = hedger;