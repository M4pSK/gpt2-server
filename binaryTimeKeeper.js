const cron = require('node-cron');
var async = require('async');
var await = require('await');
const fs = require('fs');

const Binance = require('binance-api-node').default
require('dotenv').config();

const Moment = require('moment');

//? Initiate Binary Time Keeper
//* 'node BinaryTimeKeeper.js' to run without cron timer

function BinaryTimeKeeper() {
    cron.schedule('20 * * * *', () => {
        // cron.schedule('20 * 1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31 * *', () => {
        console.log('BOT IS ON COOLDOWN');

        //? Authenticate Binance API
        const client = Binance()
        // Authenticated client, can make signed calls
        const client2 = Binance({
            apiKey: process.env.BNB_API_KEY,
            apiSecret: process.env.BNB_API_SECRET,
            // getTime: xxx // time generator function, optional, defaults to () => Date.now()
        })

        //? Reset / Normalize Data
        //* read from dbTradeStatus.json
        async function getTime() {
            //? Timestamp
            let timeStamp = Moment().format('lll');
            let unixStamp = Moment().unix();
            console.log('Time Stamp: ' + timeStamp)
            console.log('Unix Stamp: ' + unixStamp)
        }

        //? Get Previous Position
        //* read from dbTradeStatus.json
        let tradeStatusPrev = "";
        async function getPrevTradeStatus() {
            fs.readFile('./dataRun/dbTradeStatus.json', function (err, data) {
                let json = JSON.parse(data)
                let tsData = json.tradeStatus;

                //? Check if the Hedger has run and extract correct binary
                if (tsData = '1H') {
                    tradeStatusPrev = "1"
                } else if (tsData = '0H') {
                    tradeStatusPrev = "0"
                }
                else {
                    tradeStatusPrev = tsData;
                }
                console.log('TradeStatus Previous ' + tradeStatusPrev);
            })
        }

        //?Get Previous Price
        //* read from dbTradeStatus.json */
        let previousPrice = 0;
        async function getPrevPrice() {
            fs.readFile('./dataRun/dbRun0.json', function (err, data) {
                var json = JSON.parse(data)
                previousPrice = json.tradePrice;
                console.log('Previous BTC Price: ' + previousPrice);
            })
        }

        //?Get Crypto Price
        //* using Binance API */
        let currentPrice = 0;
        async function getCurrentPrice() {
            let getPrice = await client.avgPrice({ symbol: 'BTCUSDT' });
            // console.log(getPrice)
            currentPrice = getPrice.price;
            console.log('Current BTC Price: ' + currentPrice);
        }

        //? Get Position + Current Crypto Price
        //? MAKE BINARY FOR LOG NOT FOR TRADING
        let tradeStatusCur = 0;
        async function makeBinary() {
            //!//** Binary Logic */
            if (previousPrice < currentPrice) {
                tradeStatusCur = 1;
            }
            else if (previousPrice > currentPrice) {
                tradeStatusCur = 0;
            }
            else {
                tradeStatusCur = 1;
            }
            console.log('TradeStatus Current: ' + tradeStatusCur);
        };

        //? Logs TradePrice dbRun0
        //? Price since Previous hour
        //* Note: Does not effect the tradekeeper price
        //* for datalog
        async function currentPriceDbPush(priceCur) {
            let jsonDbReWrite = { "tradePrice": priceCur }
            fs.writeFileSync('./dataRun/dbRun0.json', JSON.stringify(jsonDbReWrite), 'utf-8')
        }

        let binaryLog = 0;
        let binaryInput = '';
        let GPT2Predict = '';
        let binaryLogRaw = '';
        let inputLen = 0;
        async function binaryDbPush() {
            //? Read binary Data
            fs.readFile('./dataRun/dbBinary0.json', function (err, data) {
                var jsonDbBinary = JSON.parse(data)
                binaryLogRaw = JSON.stringify(jsonDbBinary.binaryLog);

                //? Replace odd characters from Python Transfer
                let bL = binaryLogRaw.replace(/,/g, "");
                let bL0 = bL.replace('[', '');
                let bL1 = bL0.replace(']', '');
                let bL2 = bL1.replace(/['"]+/g, '');
                binaryLog = bL2.concat(tradeStatusCur.toString());

                //? Node arguments to append human Input
                if (process.argv[2] && process.argv[2] === '-up') {
                    `1${binaryLog}`
                } else if (process.argv[2] && process.argv[2] === '-down') {
                    `0${binaryLog}`
                }

                binaryInput = JSON.stringify(binaryLog);
                inputLen = binaryInput.length;

                let binaryInputJsonWrite = { "binaryLog": binaryLog }
                fs.writeFileSync('./dataRun/dbBinary0.json', JSON.stringify(binaryInputJsonWrite), 'utf-8')
                GPT2Input = JSON.stringify(binaryInputJsonWrite);

                // let GPT2PredictPos = { "tradeStatus": GPT2Predict }
                // fs.writeFileSync('./dataRun/dbTradeStatus.json', JSON.stringify(GPT2PredictPos), 'utf-8')
                // GPT2Predict = JSON.stringify(GPT2PredictPos);
            })
        }

        async.parallel([
            function (callback) {
                setTimeout(function () {
                    getTime();
                    callback(null, 1);
                }, 0);
            },
            function (callback) {
                setTimeout(function () {
                    getPrevPrice();
                    callback(null, 1);
                }, 2000);
            },
            function (callback) {
                setTimeout(function () {
                    getCurrentPrice();
                    callback(null, 1);
                }, 4000);
            },
            function (callback) {
                setTimeout(function () {
                    getPrevTradeStatus();
                    callback(null, 1);
                }, 8000);
            },
            function (callback) {
                setTimeout(function () {
                    makeBinary();
                    callback(null, 1);
                }, 9000);
            },
            function (callback) {
                setTimeout(function () {
                    binaryDbPush();
                    callback(null, 1);
                }, 10000);
            },
            function (callback) {
                setTimeout(function () {
                    currentPriceDbPush(currentPrice);
                    callback(null, 1);
                }, 11000);
            }
        ])
    })
};
module.exports = BinaryTimeKeeper;