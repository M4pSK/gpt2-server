const Binance = require('binance-api-node').default
require('dotenv').config();
const fs = require('fs')
const keys = require('./keys')

//? Authenticate Binance API
const client = Binance({
    apiKey: process.env.BNB_API_KEY,
    apiSecret: process.env.BNB_API_SECRET,
    // getTime: xxx // time generator function, optional, defaults to () => Date.now()
})

async function tradeCancel() {
    let cancelOutOrders,
        outstandingID;

    try {
        let getOutID = await client.openOrders({
            symbol: 'BTCUSDT',
        })
        outstandingID = getOutID[0].orderId
        console.log(`
            // Get Outstanding Trade //
            Order ID: ${outstandingID}`)
    } catch (error) {
        console.log('error')
        throw error
    }
    try {
        cancelOutOrders = await
            // Get Outstanding Trade //
            client.cancelOrder({
                symbol: 'BTCUSDT',
                orderId: outstandingID
            })
        console.log(`Trade Cancel`)
    } catch (error) {
        console.log('ERROR: CHECK FOR PASSED ID')
        throw error
    }
}

tradeCancel()
module.exports = tradeCancel;