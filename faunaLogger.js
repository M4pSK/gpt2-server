const cron = require('node-cron');
const Binance = require('binance-api-node').default
require('dotenv').config();;
const fs = require('fs')
const async = require('async');
const await = require('await');
const faunadb = require('faunadb')
const Moment = require('moment')

//? Authenticate Binance API
const client = Binance({
    apiKey: process.env.BNB_API_KEY,
    apiSecret: process.env.BNB_API_SECRET,
    // getTime: xxx // time generator function, optional, defaults to () => Date.now()
})

//? Authenticate FaunaDB API
const faunaClient = new faunadb.Client({ secret: 'fnADeZiZ3zACAQCsUrG7LahCUI1mvq2QF-MpDhWe' })
let q = faunadb.query

function FaunaLogger() {
    cron.schedule('53 * * * *', () => {

        let faunaDataArr = [];
        async function getFaunaData() {
            faunaClient.query(
                q.Get(q.Ref(q.Collection('c-ai-01'), '250845680626565641'))
            )
                .then((ret) => faunaDataArr = ret.data)
        }

        async function FaunaLoggerFunc() {
            //? Get Current Time
            let timeStamp = Moment().format('lll');
            let unixStamp = Moment().unix();
            console.log('Time Stamp: ' + timeStamp)
            console.log('Unix Stamp: ' + unixStamp)

            //? Get Current Fauna Data
            // console.log(`fauna Data Array ${faunaDataArr}`)
            let faunaLogUsdt = faunaDataArr.Usdt
            let faunaLogBTC = faunaDataArr.BTC
            let faunaLogTime = faunaDataArr.Time
            let faunaLogUnixTime = faunaDataArr.UnixTime
            let faunaLogPrice = faunaDataArr.Price

            //? Get BTC / USDT price from Binanace API
            let getPrice = await client.avgPrice({ symbol: 'BTCUSDT' });

            //? Get account info from Binanace API
            let data = await client.accountInfo()
            let data0 = data.balances;
            let data1 = data.balances;

            //? Filter BTC values
            let acntBTC0 = data0.filter(it => it.asset.includes('BTC'));
            let acntBTC = acntBTC0[0];

            //? Filter USDT values
            let acntUsdt0 = data1.filter(it => it.asset.includes('USDT'));
            let acntUsdt = acntUsdt0[0];
            // console.log('BTC: ' + acntBtc.free)
            // console.log('USDT: ' + acntUsdt.free)
            faunaLogUsdt.push(acntUsdt.free)
            faunaLogBTC.push(acntBTC.free)

            faunaLogTime.push(timeStamp)
            faunaLogUnixTime.push(unixStamp)

            faunaLogPrice.push(getPrice.price)

            let faunaData = {
                "Usdt": faunaLogUsdt,
                "BTC": faunaLogBTC,
                "Price": faunaLogPrice,
                "Time": faunaLogTime,
                "UnixTime": faunaLogUnixTime
            }

            let createP = faunaClient.query(
                q.Update(
                    q.Ref(q.Collection('c-ai-01'), '250845680626565641'),
                    { data: faunaData })
            )
        }
        async.parallel([
            function (callback) {
                setTimeout(function () {
                    getFaunaData()
                    callback(null, 1);
                }, 0);
            },
            function (callback) {
                setTimeout(function () {
                    FaunaLoggerFunc()
                    callback(null, 1);
                }, 1500);
            }
        ])
    })
}
// FaunaLogger()
module.exports = FaunaLogger