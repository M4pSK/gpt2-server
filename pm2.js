const pm2 = require('pm2');

function pm2Restarter() {
    pm2.connect(function (err) {
        if (err) throw err;

        setTimeout(function worker() {
            console.log("Restarting app...");
            pm2.restart('index', function () { });
            setTimeout(worker, 4000 * 60 * 60);
        }, 4000 * 60 * 60);
    })
}
pm2Restarter()